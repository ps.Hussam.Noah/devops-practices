FROM openjdk:8

LABEL HussamNoah="hussam.noah@progressoft.com"

EXPOSE 8090

COPY . usr/local/Project

ENTRYPOINT java -jar -Dspring.profiles.active=mysql /usr/local/assignment.jar
